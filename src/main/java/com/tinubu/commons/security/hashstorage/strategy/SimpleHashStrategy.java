/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;

/**
 * Simple {@link HashStrategy} that supports a simple {@link HashAlgorithm}.
 */
public class SimpleHashStrategy implements HashStrategy {

   protected final HashAlgorithm hashAlgorithm;

   public SimpleHashStrategy(HashAlgorithm hashAlgorithm) {
      this.hashAlgorithm = notNull(hashAlgorithm, "hashAlgorithm");
   }

   public HashAlgorithm hashAlgorithm() {
      return hashAlgorithm;
   }

   @Override
   public List<HashAlgorithm> hashAlgorithms() {
      return singletonList(hashAlgorithm);
   }

   @Override
   public byte[] hash(byte[] payload) {
      return hashAlgorithm.hash(payload);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SimpleHashStrategy that = (SimpleHashStrategy) o;
      return Objects.equals(hashAlgorithm, that.hashAlgorithm);
   }

   @Override
   public int hashCode() {
      return Objects.hash(hashAlgorithm);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SimpleHashStrategy.class.getSimpleName() + "[", "]")
            .add("hashAlgorithm=" + hashAlgorithm)
            .toString();
   }

}
