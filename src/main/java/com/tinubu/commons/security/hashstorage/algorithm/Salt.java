/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.StringJoiner;

import com.tinubu.commons.security.hashstorage.crypto.Random;

/**
 * Represents a salt.
 * Internally, the salt is stored as a byte array, not a representable string.
 */
public class Salt {

   private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
   private static final Random PSEUDO_RANDOM_GENERATOR = new Random("SHA1PRNG");

   private final byte[] salt;

   private Salt(byte[] salt) {
      this.salt = notEmpty(salt, "salt");
   }

   /**
    * Creates a new {@link Salt} from byte array.
    *
    * @param salt salt
    *
    * @return new salt
    */
   public static Salt ofBytes(byte[] salt) {
      return new Salt(notNull(salt, "salt"));
   }

   /**
    * Creates a new {@link Salt} from string.
    *
    * @param salt salt
    * @param charset charset to use to convert string to salt byte array
    *
    * @return new salt
    */
   public static Salt ofString(String salt, Charset charset) {
      notNull(salt, "salt");
      notNull(charset, "charset");

      return new Salt(salt.getBytes(charset));
   }

   /**
    * Creates a new {@link Salt} from string, using {@link #DEFAULT_CHARSET default charset} to convert string
    * to salt byte array.
    *
    * @param salt salt
    *
    * @return new salt
    */
   public static Salt ofString(String salt) {
      return ofString(notNull(salt, "salt"), DEFAULT_CHARSET);
   }

   /**
    * Creates a new {@link Salt} from a pseudo-random generator.
    *
    * @param length length of the salt to generate in bytes
    *
    * @return new salt
    */
   public static Salt pseudoRandom(int length) {
      if (length < 0) {
         throw new IllegalArgumentException("salt length must be > 0");
      }

      return new Salt(PSEUDO_RANDOM_GENERATOR.random(length));
   }

   /**
    * Creates a new {@link Salt} from the specified random generator.
    *
    * @param length length of the salt to generate in bytes
    *
    * @return new salt
    */
   public static Salt pseudoRandom(SecureRandom randomGenerator, int length) {
      notNull(randomGenerator, "randomGenerator");
      if (length < 0) {
         throw new IllegalArgumentException("salt length must be > 0");
      }

      return new Salt(new Random(randomGenerator).random(length));
   }

   /**
    * Returns the salt as a byte array.
    *
    * @return the salt as a byte array
    */
   public byte[] asBytes() {
      return salt;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Salt salt1 = (Salt) o;
      return Arrays.equals(salt, salt1.salt);
   }

   @Override
   public int hashCode() {
      return Arrays.hashCode(salt);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Salt.class.getSimpleName() + "[", "]")
            .add("salt=" + Arrays.toString(salt))
            .toString();
   }

}
