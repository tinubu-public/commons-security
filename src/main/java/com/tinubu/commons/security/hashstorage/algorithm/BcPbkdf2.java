/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Objects.requireNonNull;

import java.util.Optional;
import java.util.function.Supplier;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;

import com.tinubu.commons.security.hashstorage.algorithm.HashParameters.HashParametersBuilder;

/**
 * BouncyCastle Password-Based Key Derivation Function 2 (PBKDF2) hash algorithm abstract implementation.
 */
public abstract class BcPbkdf2 implements HashAlgorithm {

   /**
    * Iterations parameter name.
    */
   public static final String ITERATIONS_PARAMETER = "it";
   public static final String KEY_LENGTH_PARAMETER = "kl";

   protected final Supplier<Digest> digest;
   protected final Salt salt;
   protected final int iterations;
   protected final int keyLength;

   protected BcPbkdf2(Supplier<Digest> digest, Salt salt, int iterations, int keyLength) {
      this.digest = notNull(digest, "digest");
      this.salt = notNull(salt, "salt");
      this.iterations = strictlyPositiveValue(iterations, "iterations");
      this.keyLength = strictlyPositiveValue(keyLength, "keyLength");
   }

   @Override
   public Optional<Salt> salt() {
      return Optional.of(salt);
   }

   public int iterations() {
      return iterations;
   }

   public int keyLength() {
      return keyLength;
   }

   @Override
   public byte[] hash(byte[] payload) {
      notNull(payload, "payload");

      Digest digestInstance = requireNonNull(this.digest.get());
      PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(digestInstance);
      gen.init(payload, salt.asBytes(), iterations);
      byte[] hash = ((KeyParameter) gen.generateDerivedParameters(keyLength)).getKey();

      digestInstance.reset();

      return hash;
   }

   @Override
   public HashParameters parameters() {
      HashParametersBuilder parameters = new HashParametersBuilder();

      parameters.parameter(ITERATIONS_PARAMETER, String.valueOf(iterations));
      parameters.parameter(KEY_LENGTH_PARAMETER, String.valueOf(keyLength));

      return parameters.build();
   }

   protected static int strictlyPositiveValue(final int value, final String name) {
      if (value <= 0) {
         throw new IllegalArgumentException(String.format("'%s' must be > 0", name));
      }
      return value;
   }

   @SuppressWarnings("unchecked")
   public abstract static class BcPkbdf2Builder<T extends BcPkbdf2Builder<T>> implements HashAlgorithmBuilder {
      protected Salt salt;
      protected Integer iterations;
      protected Integer keyLength;

      public T salt(Salt salt) {
         this.salt = salt;
         return (T) this;
      }

      public T iterations(Integer iterations) {
         this.iterations = iterations;
         return (T) this;
      }

      public T keyLength(Integer keyLength) {
         this.keyLength = keyLength;
         return (T) this;
      }
   }

}
