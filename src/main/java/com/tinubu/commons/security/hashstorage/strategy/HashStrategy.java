/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import java.util.List;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;

/**
 * Provides an abstraction over {@link HashAlgorithm} to implement arbitrary hashing strategy.
 */
public interface HashStrategy {

   /**
    * Returns the list of hash algorithms used to hash the payload, ordered from the first algorithm to the
    * last algorithm applied to payload. For example, the composed strategy {@code sha1(md5(payload))} will
    * return the list {@code [md5, sha1]}.
    *
    * @return the list of hash algorithms
    */
   List<HashAlgorithm> hashAlgorithms();

   /**
    * Generates a hash by applying the current strategy on specified payload.
    *
    * @param payload initial payload to apply this strategy
    *
    * @return generated hash
    */
   byte[] hash(byte[] payload);

}
