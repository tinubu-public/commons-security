/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * MD5 {@link HashAlgorithm} implementation.
 */
public class Md5HashAlgorithm extends MessageDigestHashAlgorithm implements HashAlgorithm {
   private static final List<String> ALGORITHM_NAMES = Arrays.asList("md5", "1");

   /**
    * MD5 algorithm {@link MessageDigest} instance name.
    */
   public static final String MD5_ALGORITHM = "MD5";

   private Md5HashAlgorithm(Md5HashAlgorithmBuilder builder) {
      super(MD5_ALGORITHM, builder.salt);
   }

   @Override
   public List<String> algorithmNames() {
      return ALGORITHM_NAMES;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Md5HashAlgorithm that = (Md5HashAlgorithm) o;
      return Objects.equals(salt, that.salt);
   }

   @Override
   public int hashCode() {
      return Objects.hash(salt);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Md5HashAlgorithm.class.getSimpleName() + "[", "]").add("salt=" + salt).toString();
   }

   public static class Md5HashAlgorithmBuilder
         extends MessageDigestHashAlgorithmBuilder<Md5HashAlgorithmBuilder> {
      @Override
      public Md5HashAlgorithm build() {
         return new Md5HashAlgorithm(this);
      }
   }

   public static class Md5HashAlgorithmProvider implements HashAlgorithmProvider {

      @Override
      public List<String> algorithmNames() {
         return ALGORITHM_NAMES;
      }

      @Override
      public Md5HashAlgorithm fromFormat(Salt salt, Map<String, String> parameters) {
         notNull(parameters, "parameters");

         return new Md5HashAlgorithmBuilder().salt(salt).build();
      }
   }

}
