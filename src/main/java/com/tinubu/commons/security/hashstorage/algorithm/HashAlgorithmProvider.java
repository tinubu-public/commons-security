/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.List;
import java.util.Map;

import com.tinubu.commons.security.hashstorage.format.HashFormat;

public interface HashAlgorithmProvider {

   /**
    * Algorithm name and aliases. This names will be used in {@link HashFormat}, potentially normalized, to
    * identify hash algorithm.
    * You should use conservatively [a-z0-9] characters for this name.
    * The list must contain at least one name, the first name of the list will be used to generate new hash
    * formats.
    *
    * @return hash algorithm names
    */
   List<String> algorithmNames();

   /**
    * Reconstitutes a {@link HashAlgorithm} from format parameters. The implementation should check for
    * mandatory parameters, that can be optional in another context, using {@link
    * #requireSalt(Salt)} or {@link #requireParameter(Map, String)}.
    *
    * @param salt salt from format, or {@code null}
    * @param parameters format parameters
    *
    * @return new hash algorithm
    */
   HashAlgorithm fromFormat(Salt salt, Map<String, String> parameters);

   /**
    * Checks for salt.
    *
    * @param salt salt to check
    *
    * @return checked salt value
    */
   default Salt requireSalt(Salt salt) {
      return nullable(salt).orElseThrow(() -> new IllegalArgumentException("No salt set"));
   }

   /**
    * Checks for parameter.
    *
    * @param parameters known parameters
    * @param parameter parameter to check
    *
    * @return checked parameter value
    */
   default String requireParameter(Map<String, String> parameters, String parameter) {
      return nullable(parameters.get(parameter)).orElseThrow(() -> new IllegalArgumentException(String.format(
            "No '%s' parameter set",
            parameter)));
   }

}
