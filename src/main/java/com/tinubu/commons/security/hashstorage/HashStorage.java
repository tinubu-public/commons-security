/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage;

import static com.tinubu.commons.lang.model.ImmutableUtils.immutableList;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.format.HashFormat;
import com.tinubu.commons.security.hashstorage.strategy.ComposedHashStrategy;
import com.tinubu.commons.security.hashstorage.strategy.HashStrategy;
import com.tinubu.commons.security.hashstorage.strategy.SimpleHashStrategy;

/**
 * Defines a representation of a hash with the applied hash algorithms.
 */
public class HashStorage {

   private final List<HashAlgorithm> hashAlgorithms;
   private final byte[] hash;

   private HashStorage(final List<HashAlgorithm> hashAlgorithms, final byte[] hash) {
      this.hashAlgorithms = immutableList(notEmpty(noNullElements(hashAlgorithms, "hashAlgorithms"), "hashAlgorithms"));
      this.hash = notEmpty(hash, "hash");
   }

   /**
    * Creates a {@link HashStorage} from a {@link HashStrategy} and a resulting hash.
    *
    * @param hashStrategy hash strategy used to generate the hash
    * @param hash resulting hash
    *
    * @return new hash storage
    */
   public static HashStorage ofHash(final HashStrategy hashStrategy, final byte[] hash) {
      notNull(hashStrategy, "hashStrategy");

      return new HashStorage(hashStrategy.hashAlgorithms(), hash);
   }

   /**
    * Shortcut for {@link #ofHash(HashStrategy, byte[])} using a simple hash algorithm.
    *
    * @param hashAlgorithm hash algorithm used to generate the hash
    * @param hash resulting hash
    *
    * @return new hash storage
    */
   public static HashStorage ofHash(final HashAlgorithm hashAlgorithm, final byte[] hash) {
      return ofHash(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")), hash);
   }

   /**
    * Creates a {@link HashStorage} by applying a {@link HashStrategy} to the specified payload.
    *
    * @param hashStrategy hash strategy to use to generate the hash
    * @param payload payload to apply the hash strategy to
    *
    * @return new hash storage
    */
   public static HashStorage ofPayload(final HashStrategy hashStrategy, final byte[] payload) {
      notNull(hashStrategy, "hashStrategy");

      return HashStorage.ofHash(hashStrategy, hashStrategy.hash(payload));
   }

   /**
    * Shortcut for {@link #ofPayload(HashStrategy, byte[])} using a simple hash algorithm.
    *
    * @param hashAlgorithm hash algorithm to use to generate the hash
    * @param payload payload to apply the hash strategy to
    *
    * @return new hash storage
    */
   public static HashStorage ofPayload(final HashAlgorithm hashAlgorithm, final byte[] payload) {
      return ofPayload(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")), payload);
   }

   /**
    * Creates a {@link HashStorage} from a string representation using specified {@link HashFormat} parser.
    *
    * @param hashFormat hash format used to parse the string representation
    * @param format hash string representation
    *
    * @return new hash storage
    */
   public static HashStorage ofFormat(final HashFormat hashFormat, final String format) {
      notNull(hashFormat, "hashFormat");
      notBlank(format, "format");

      return hashFormat.parse(format);
   }

   /**
    * Verify the specified {@link HashStorage} against this hash storage.
    * To be successfully verified, the following rules are checked :
    * <ul>
    *    <li>salt must be equal</li>
    *    <li>the hash algorithm list must exactly match in the same order and with the same hash parameters</li>
    * </ul>
    *
    * @param hashStorage hash storage to compare
    *
    * @return {@code true} if specified hash storage is successfully verified
    */
   public boolean verify(final HashStorage hashStorage) {
      notNull(hashStorage, "hashStorage");

      return Objects.equals(hashAlgorithms, hashStorage.hashAlgorithms) && Arrays.equals(hash,
                                                                                         hashStorage.hash);
   }

   /**
    * Verifies that the specified payload, hashed with this object's hash strategy, verify this hash storage.
    *
    * @param payload payload to hash
    *
    * @return {@code true} if hashed payload is successfully verified
    */
   public boolean verify(final byte[] payload) {
      notNull(payload, "payload");

      return verify(HashStorage.ofPayload(ComposedHashStrategy.ofHashAlgorithms(hashAlgorithms), payload));
   }

   /**
    * Re-hash the current {@link HashStorage} with another hash strategy.
    *
    * @param hashStrategy hash strategy to use to re-hash the current hash
    *
    * @return new re-hashed hash storage instance
    */
   public HashStorage rehash(final HashStrategy hashStrategy) {
      ComposedHashStrategy reHashStrategy =
            new ComposedHashStrategy(hashStrategy).preApplied(ComposedHashStrategy.ofHashAlgorithms(
                  hashAlgorithms));

      return HashStorage.ofPayload(reHashStrategy, this.hash);
   }

   /**
    * Shortcut for {@link #rehash(HashStrategy)} using a simple hash algorithm.
    *
    * @param hashAlgorithm hash hashAlgorithm to use to re-hash the current hash
    *
    * @return new re-hashed hash storage instance
    */
   public HashStorage rehash(final HashAlgorithm hashAlgorithm) {
      return rehash(new SimpleHashStrategy(notNull(hashAlgorithm, "hashAlgorithm")));
   }

   /**
    * Returns a string representation of this hash storage using specified {@link HashFormat}.
    *
    * @param format hash format to use to generate the string representation
    *
    * @return a string representation of this hash storage using specified hash format
    */
   public String format(final HashFormat format) {
      notNull(format, "format");

      return format.format(this);
   }

   /**
    * Returns the hash algorithms used to generate the {@link #hash()} as an ordered list from the first
    * algorithm to the last algorithm applied to payload. For example, the composed hashing
    * {@code sha1(md5(payload))} will be represented as the list {@code [md5, sha1]}.
    *
    * @return the hash algorithms used to generate the hash
    */
   public List<HashAlgorithm> hashAlgorithms() {
      return hashAlgorithms;
   }

   /**
    * Returns {@code true} if used hash algorithms exactly matches, in order, the specified algorithms.
    *
    * @param hashAlgorithms ordered list of hash algorithms to match
    *
    * @return {@code true} if used hash algorithms exactly matches, in order, the specified algorithms
    */
   public boolean usesHashAlgorithms(final List<HashAlgorithm> hashAlgorithms) {
      notEmpty(noNullElements(hashAlgorithms, "hashAlgorithms"), "hashAlgorithms");

      return this.hashAlgorithms.equals(hashAlgorithms);
   }

   /**
    * Returns {@code true} if used hash algorithms exactly matches, in order, the specified algorithms.
    *
    * @param hashAlgorithms ordered list of hash algorithms to match
    *
    * @return {@code true} if used hash algorithms exactly matches, in order, the specified algorithms
    */
   public boolean usesHashAlgorithms(final HashAlgorithm... hashAlgorithms) {
      return usesHashAlgorithms(Arrays.asList(notNull(hashAlgorithms, "hashAlgorithms")));
   }

   /**
    * Returns the hash.
    *
    * @return the hash
    */
   public byte[] hash() {
      return hash;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      HashStorage that = (HashStorage) o;
      return Objects.equals(hashAlgorithms, that.hashAlgorithms) && Arrays.equals(hash, that.hash);
   }

   @Override
   public int hashCode() {
      int result = Objects.hash(hashAlgorithms);
      result = 31 * result + Arrays.hashCode(hash);
      return result;
   }

}
