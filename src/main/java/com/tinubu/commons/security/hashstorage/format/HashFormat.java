/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import com.tinubu.commons.security.hashstorage.HashStorage;

/**
 * Defines a string representation of a {@link HashStorage}.
 * Implementations support formatting and parsing to/from string representation.
 */
public interface HashFormat {

   /**
    * Returns {@code true} if this formatter supports multiple applied hash algorithms.
    *
    * @return {@code true} if this formatter supports multiple applied hash algorithms
    */
   boolean supportMultipleHashAlgorithms();

   /**
    * Formats a {@link HashStorage} to a string representation
    *
    * @param hashStorage hash storage to format
    *
    * @return string representation
    */
   String format(HashStorage hashStorage);

   /**
    * Parses a string representation to a {@link HashStorage}.
    *
    * @param hashFormat string representation to parse
    *
    * @return hash storage
    */
   HashStorage parse(String hashFormat);
}
