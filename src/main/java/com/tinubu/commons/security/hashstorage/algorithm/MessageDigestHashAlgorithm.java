/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import com.tinubu.commons.security.hashstorage.algorithm.HashParameters.HashParametersBuilder;

/**
 * {@link MessageDigest} hash algorithm abstract implementation.
 */
public abstract class MessageDigestHashAlgorithm implements HashAlgorithm {

   protected final String messageDigestAlgorithm;
   protected final Salt salt;

   public MessageDigestHashAlgorithm(String messageDigestAlgorithm, Salt salt) {
      this.messageDigestAlgorithm = notBlank(messageDigestAlgorithm, "messageDigestAlgorithm");
      this.salt = salt;
   }

   @Override
   public byte[] hash(byte[] payload) {
      notNull(payload, "payload");

      MessageDigest md = messageDigest();
      salt().map(Salt::asBytes).ifPresent(md::update);

      byte[] hash = md.digest(payload);
      md.reset();

      return hash;
   }

   @Override
   public Optional<Salt> salt() {
      return nullable(salt);
   }

   @Override
   public HashParameters parameters() {
      return new HashParametersBuilder().build();
   }

   private MessageDigest messageDigest() {
      try {
         return MessageDigest.getInstance(messageDigestAlgorithm);
      } catch (NoSuchAlgorithmException e) {
         throw new IllegalStateException(String.format("Unknown '%s' algorithm", messageDigestAlgorithm), e);
      }
   }

   @SuppressWarnings("unchecked")
   public abstract static class MessageDigestHashAlgorithmBuilder<T extends MessageDigestHashAlgorithmBuilder<T>>
         implements HashAlgorithmBuilder {

      protected Salt salt;

      public T salt(Salt salt) {
         this.salt = salt;
         return (T) this;
      }
   }

}
