/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Optional;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.tinubu.commons.security.hashstorage.algorithm.HashParameters.HashParametersBuilder;

/**
 * {@link SecretKeyFactory} hash algorithm abstract implementation.
 * <p>
 * This abstract implementation is better suited for character based payloads because of {@link PBEKeySpec}
 * implementation.
 */
public abstract class SecretKeyHashAlgorithm implements HashAlgorithm {

   /**
    * Iterations parameter name.
    */
   public static final String ITERATIONS_PARAMETER = "it";
   public static final String KEY_LENGTH_PARAMETER = "kl";

   /**
    * Default charset for payload input.
    */
   private static final Charset DEFAULT_PAYLOAD_CHARSET = StandardCharsets.UTF_8;

   protected final String secretKeyAlgorithm;
   protected final Salt salt;
   protected final int iterations;
   protected final int keyLength;
   protected final Charset payloadCharset;

   protected SecretKeyHashAlgorithm(String secretKeyAlgorithm,
                                    Salt salt,
                                    int iterations,
                                    int keyLength,
                                    Charset payloadCharset) {
      this.secretKeyAlgorithm = notBlank(secretKeyAlgorithm, "secretKeyAlgorithm");
      this.salt = notNull(salt, "salt");
      this.iterations = strictlyPositiveValue(iterations, "iterations");
      this.keyLength = strictlyPositiveValue(keyLength, "keyLength");
      this.payloadCharset = nullable(payloadCharset, DEFAULT_PAYLOAD_CHARSET);
   }

   @Override
   public Optional<Salt> salt() {
      return Optional.of(salt);
   }

   public int iterations() {
      return iterations;
   }

   public int keyLength() {
      return keyLength;
   }

   public Charset payloadCharset() {
      return payloadCharset;
   }

   /**
    * {@inheritDoc}
    * <p>
    * Important : Because of an implementation limitation in {@link PBEKeySpec}, the input payload must be
    * encoded with configured {@link #payloadCharset}. Payload encoding is validated against configured
    * charset.
    */
   @Override
   public byte[] hash(byte[] payload) {
      notNull(payload, "payload");

      char[] password = payloadCharArray(payload);

      try {
         PBEKeySpec pbeKeySpec = new PBEKeySpec(password, salt.asBytes(), iterations, keyLength);
         byte[] hash = secretKeyFactory(secretKeyAlgorithm).generateSecret(pbeKeySpec).getEncoded();
         pbeKeySpec.clearPassword();

         return hash;
      } catch (InvalidKeySpecException e) {
         throw new IllegalStateException(e);
      }
   }

   @Override
   public HashParameters parameters() {
      HashParametersBuilder parameters = new HashParametersBuilder();

      parameters.parameter(ITERATIONS_PARAMETER, String.valueOf(iterations));
      parameters.parameter(KEY_LENGTH_PARAMETER, String.valueOf(keyLength));

      return parameters.build();
   }

   protected static int strictlyPositiveValue(final int value, final String name) {
      if (value <= 0) {
         throw new IllegalArgumentException(String.format("'%s' must be > 0", name));
      }
      return value;
   }

   private static SecretKeyFactory secretKeyFactory(String algorithm) {
      try {
         return SecretKeyFactory.getInstance(algorithm);
      } catch (NoSuchAlgorithmException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Checks payload has been correctly encoded as specified in {@link #payloadCharset}. {@link PBEKeySpec}
    * requires payload to be a char[], so we must be able to convert byte[] payload to char[] using specified
    * charset consistently (without removing or replacing characters during conversion).
    */
   private char[] payloadCharArray(byte[] payload) {
      String stringPayload = new String(payload, payloadCharset);

      if (!Arrays.equals(stringPayload.getBytes(payloadCharset), payload)) {
         throw new IllegalArgumentException(String.format(
               "Input payload must be encoded in %s. You can configure an alternative payload charset",
               payloadCharset.displayName()));
      }

      return stringPayload.toCharArray();
   }

   @SuppressWarnings("unchecked")
   public abstract static class SecretKeyHashAlgorithmBuilder<T extends SecretKeyHashAlgorithmBuilder<T>>
         implements HashAlgorithmBuilder {
      protected Salt salt;
      protected Integer iterations;
      protected Integer keyLength;
      protected Charset payloadCharset;

      public T salt(Salt salt) {
         this.salt = salt;
         return (T) this;
      }

      public T iterations(Integer iterations) {
         this.iterations = iterations;
         return (T) this;
      }

      public T keyLength(Integer keyLength) {
         this.keyLength = keyLength;
         return (T) this;
      }

      public T payloadCharset(Charset payloadCharset) {
         this.payloadCharset = payloadCharset;
         return (T) this;
      }

   }

}
