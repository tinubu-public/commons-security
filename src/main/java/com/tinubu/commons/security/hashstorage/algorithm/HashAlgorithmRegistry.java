/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.tinubu.commons.security.hashstorage.format.HashFormat;

/**
 * Hash algorithms registry.
 *
 * @implSpec Thread-safe, immutable implementation.
 */
public final class HashAlgorithmRegistry {
   /**
    * Factory instance singleton.
    */
   private static final HashAlgorithmRegistry INSTANCE = new HashAlgorithmRegistry();

   private static final Map<String, HashAlgorithmProvider> hashAlgorithms = algorithmServices();

   private HashAlgorithmRegistry() {}

   /**
    * Returns registry singleton.
    *
    * @return registry singleton
    */
   public static HashAlgorithmRegistry instance() {
      return INSTANCE;
   }

   private static Map<String, HashAlgorithmProvider> algorithmServices() {
      ServiceLoader<HashAlgorithmProvider> algorithmServiceLoader =
            ServiceLoader.load(HashAlgorithmProvider.class);

      Map<String, HashAlgorithmProvider> hashAlgorithms = new ConcurrentHashMap<>();

      for (HashAlgorithmProvider hashAlgorithmProvider : algorithmServiceLoader) {
         for (String algorithmName : hashAlgorithmProvider.algorithmNames()) {
            hashAlgorithms.put(normalizedIndex(algorithmName), hashAlgorithmProvider);
         }
      }

      return hashAlgorithms;
   }

   /**
    * Registers a new hash algorithm.
    *
    * @param hashAlgorithmProvider the hash algorithm provider
    */
   public void registerHashAlgorithm(HashAlgorithmProvider hashAlgorithmProvider) {
      notNull(hashAlgorithmProvider, "hashAlgorithmProvider");

      for (String algorithmName : hashAlgorithmProvider.algorithmNames()) {
         hashAlgorithms.put(normalizedIndex(algorithmName), hashAlgorithmProvider);
      }
   }

   /**
    * Clears the list of registered hash algorithms.
    */
   public void clearRegisteredHashAlgorithmProviders() {
      hashAlgorithms.clear();
   }

   /**
    * Returns the list of registered hash algorithms as a set because of aliases.
    *
    * @return registered hash algorithm providers
    */
   public Set<HashAlgorithmProvider> registeredHashAlgorithmProviders() {
      return new HashSet<>(hashAlgorithms.values());

   }

   /**
    * Returns the hash algorithm specified by algorithm name.
    *
    * @param hashAlgorithm the given hash algorithm
    *
    * @return hash algorithm provider or {@link Optional#empty}
    */
   public Optional<HashAlgorithmProvider> hashAlgorithmProvider(String hashAlgorithm) {
      notBlank(hashAlgorithm, "hashAlgorithm");

      return nullable(hashAlgorithms.get(normalizedIndex(hashAlgorithm)));
   }

   /**
    * Hash algorithm supported characters are not enforced in core library but in each {@link HashFormat}. We
    * internally use a minified index ([a-z0-9]) to enable hash format to transform hash algorithm name and
    * the indexed search in registry to continue to work.
    */
   private static String normalizedIndex(String hashAlgorithm) {
      return hashAlgorithm.toLowerCase().replaceAll("[^a-z0-9]+", "");
   }
}
