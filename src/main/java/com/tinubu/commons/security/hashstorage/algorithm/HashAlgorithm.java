/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import java.util.List;
import java.util.Optional;

import com.tinubu.commons.security.hashstorage.format.HashFormat;

/**
 * Defines a hash algorithm.
 * A hash algorithm transforms a <em>payload</em> into a <em>hash</em> deterministically given an optional
 * salt and parameters.
 * <p>
 * Hash algorithm instances must be stateless, so that {@link #hash(byte[])} can be called several times.
 */
public interface HashAlgorithm {

   /**
    * Algorithm name and aliases. This names will be used in {@link HashFormat}, potentially normalized, to
    * identify hash algorithm.
    * You should use conservatively [a-z0-9] characters for this name.
    * The list must contain at least one name, the first name of the list will be used to generate new hash
    * formats.
    *
    * @return hash algorithm names
    */
   List<String> algorithmNames();

   /**
    * Algorithm name. This name will be used to generate new hash formats.
    *
    * @return hash algorithm names
    */
   default String algorithmName() {
      return algorithmNames().get(0);
   }

   /**
    * Hashes specified payload with this hash algorithm.
    *
    * @param payload payload to hash
    *
    * @return hash
    *
    * @implSpec Implementations should explicitly clear payload copies from memory, once hash is
    *       computed, by replacing memory area with junk characters.
    */
   byte[] hash(byte[] payload);

   /**
    * Returns the optional salt configured for this algorithm.
    *
    * @return configured solt or {@link Optional#empty()} if no salt is configured.
    */
   Optional<Salt> salt();

   /**
    * Returns parameters for this hash algorithm.
    *
    * @return parameters for this hash algorithm
    */
   HashParameters parameters();

}
