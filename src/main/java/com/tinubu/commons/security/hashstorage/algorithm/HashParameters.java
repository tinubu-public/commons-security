/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.Set;

/**
 * Represents a set of hash algorithm parameters.
 *
 * @implSpec Parameters insertion order is maintained.
 */
public class HashParameters extends AbstractMap<String, String> {

   private final LinkedHashMap<String, String> parameters;

   private HashParameters(HashParametersBuilder builder) {
      this.parameters = notNull(builder.parameters, "parameters");
   }

   /**
    * Returns hash algorithm parameter value for specified parameter name.
    *
    * @return parameter value or {@link Optional#empty()} if parameter does not exist
    */
   public Optional<String> parameter(String name) {
      return nullable(parameters.get(name));
   }

   @Override
   public Set<Entry<String, String>> entrySet() {
      return parameters.entrySet();
   }

   public static class HashParametersBuilder {
      private LinkedHashMap<String, String> parameters = new LinkedHashMap<>();

      public static HashParameters empty() {
         return new HashParametersBuilder().build();
      }

      public HashParametersBuilder parameters(LinkedHashMap<String, String> parameters) {
         this.parameters = parameters;

         return this;
      }

      public HashParametersBuilder parameter(String name, String value) {
         notNull(name, "name");
         notNull(value, "value");

         parameters.put(name, value);

         return this;
      }

      public HashParameters build() {
         return new HashParameters(this);
      }
   }
}
