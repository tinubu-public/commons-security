/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.HashStorage;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithmRegistry;
import com.tinubu.commons.security.hashstorage.algorithm.HashParameters;
import com.tinubu.commons.security.hashstorage.algorithm.HashParameters.HashParametersBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.Pbkdf2HmacSha256HashAlgorithm.Pbkdf2HmacSha256HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Pbkdf2HmacSha256HashAlgorithm.Pbkdf2HmacSha256HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.Salt;
import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.strategy.ComposedHashStrategy;

class ComposedHashFormatTest {

   @BeforeEach
   public void beforeEach() {
      HashAlgorithmRegistry registry = HashAlgorithmRegistry.instance();

      registry.clearRegisteredHashAlgorithmProviders();
      registry.registerHashAlgorithm(new Md5HashAlgorithmProvider());
      registry.registerHashAlgorithm(new Sha1HashAlgorithmProvider());
      registry.registerHashAlgorithm(new Pbkdf2HmacSha256HashAlgorithmProvider());
   }

   @Test
   public void testFormatWhenNominal() {
      HashStorage hashStorage =
            HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

      ComposedHashFormat hashFormat = new ComposedHashFormat();
      assertThat(hashFormat.supportMultipleHashAlgorithms()).isTrue();
      assertThat(hashFormat.format(hashStorage)).isEqualTo("{md5}Mhw89IbtUJFk7eweGYH+yA");
   }

   @Test
   public void testFormatWhenMultipleHashAlgorithms() {
      HashStorage hashStorage =
            HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build()),
                                  payload("payload"));

      assertThat(new ComposedHashFormat().format(hashStorage)).isEqualTo("{md5}{sha1}GWQE5kJShLYwuCnKvLPYzA");
   }

   @Test
   public void testFormatWhenParameters() {
      HashStorage hashStorage = HashStorage.ofPayload(new Pbkdf2HmacSha256HashAlgorithmBuilder()
                                                            .salt(Salt.ofBytes(new byte[] { 22, -33, 42, 5 }))
                                                            .keyLength(128)
                                                            .iterations(1000)
                                                            .build(), payload("payload"));

      assertThat(new ComposedHashFormat().format(hashStorage)).isEqualTo(
            "{pbkdf2hmacsha256;salt=Ft8qBQ;it=1000;kl=128}3m3oquHQEoG0jAenBmlCjA");
   }

   @Test
   public void testFormatWhenUnfulfilledHashAlgorithm() {
      HashStorage hashStorage = HashStorage.ofPayload(new HashAlgorithm() {
         @Override
         public List<String> algorithmNames() {
            return singletonList("_AlGoRiThM-NaMe_");
         }

         @Override
         public byte[] hash(byte[] payload) {
            return new byte[] { 42 };
         }

         @Override
         public Optional<Salt> salt() {
            return Optional.empty();
         }

         @Override
         public HashParameters parameters() {
            return HashParametersBuilder.empty();
         }
      }, payload("payload"));

      assertThat(new ComposedHashFormat().format(hashStorage)).isEqualTo("{algorithmname}Kg");
   }

   @Test
   public void testFormatWhenNullHashStorage() {
      assertThatNullPointerException().isThrownBy(() -> new ComposedHashFormat().format(null)).withMessage("'hashStorage' must not be null");
   }

   @Test
   public void testParseWhenNominal() {
      HashStorage hashStorage = new ComposedHashFormat().parse("{md5}Mhw89IbtUJFk7eweGYH+yA");

      assertThat(hashStorage).isNotNull();
      assertThat(hashStorage.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build());
      assertThat(hashStorage.hash()).isEqualTo(new byte[] {
            50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
   }

   @Test
   public void testParseWhenBlankFormat() {
      assertThatNullPointerException().isThrownBy(() -> new ComposedHashFormat().parse(null)).withMessage("'hashFormat' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> new ComposedHashFormat().parse("")).withMessage("'hashFormat' must not be blank");
      assertThatIllegalArgumentException().isThrownBy(() -> new ComposedHashFormat().parse(" ")).withMessage("'hashFormat' must not be blank");
   }

   @Test
   public void testParseWhenComposedStrategy() {
      HashStorage hashStorage = new ComposedHashFormat().parse("{md5}{sha1}GWQE5kJShLYwuCnKvLPYzA");

      assertThat(hashStorage).isNotNull();
      assertThat(hashStorage.hashAlgorithms()).containsExactly(new Sha1HashAlgorithmBuilder().build(),
                                                               new Md5HashAlgorithmBuilder().build());
      assertThat(hashStorage.hash()).isEqualTo(new byte[] {
            25, 100, 4, -26, 66, 82, -124, -74, 48, -72, 41, -54, -68, -77, -40, -52 });
   }

   @Test
   public void testParseWhenParameters() {
      HashStorage hashStorage = new ComposedHashFormat().parse(
            "{pbkdf2hmacsha256;salt=Ft8qBQ;it=1000;kl=128}3m3oquHQEoG0jAenBmlCjA");

      assertThat(hashStorage).isNotNull();
      assertThat(hashStorage.hashAlgorithms()).containsExactly(new Pbkdf2HmacSha256HashAlgorithmBuilder()
                                                                     .salt(Salt.ofBytes(new byte[] {
                                                                           22, -33, 42, 5 }))
                                                                     .keyLength(128)
                                                                     .iterations(1000)
                                                                     .build());
      assertThat(hashStorage.hash()).isEqualTo(new byte[] {
            -34, 109, -24, -86, -31, -48, 18, -127, -76, -116, 7, -89, 6, 105, 66, -116 });
   }

   @Test
   public void testParseWhenInvalidParameters() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComposedHashFormat().parse(
                  "{pbkdf2hmacsha256;salt=Ft8qBQ;it=;kl=128}3m3oquHQEoG0jAenBmlCjA"))
            .withMessage("Bad format : {pbkdf2hmacsha256;salt=Ft8qBQ;it=;kl=128}3m3oquHQEoG0jAenBmlCjA");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComposedHashFormat().parse(
                  "{pbkdf2hmacsha256;salt=Ft8qBQ;it;kl=128}3m3oquHQEoG0jAenBmlCjA"))
            .withMessage("Bad format : {pbkdf2hmacsha256;salt=Ft8qBQ;it;kl=128}3m3oquHQEoG0jAenBmlCjA");
      assertThatIllegalArgumentException().isThrownBy(() -> new ComposedHashFormat().parse(
            "{pbkdf2hmacsha256;salt=Ft8qBQ;it=STRING;kl=128}3m3oquHQEoG0jAenBmlCjA"));
   }

   @Test
   public void testParseWhenParameterSeparatorInValue() {
      HashStorage hashStorage = new ComposedHashFormat().parse(
            "{pbkdf2hmacsha256;salt=Ft8qBQ==;it=1000;kl=128}3m3oquHQEoG0jAenBmlCjA");

      assertThat(hashStorage).isNotNull();
      assertThat(hashStorage.hashAlgorithms()).containsExactly(new Pbkdf2HmacSha256HashAlgorithmBuilder()
                                                                     .salt(Salt.ofBytes(new byte[] {
                                                                           22, -33, 42, 5 }))
                                                                     .keyLength(128)
                                                                     .iterations(1000)
                                                                     .build());
      assertThat(hashStorage.hash()).isEqualTo(new byte[] {
            -34, 109, -24, -86, -31, -48, 18, -127, -76, -116, 7, -89, 6, 105, 66, -116 });
   }

   @Test
   public void testParseWhenNoMetadata() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComposedHashFormat().parse("3m3oquHQEoG0jAenBmlCjA"))
            .withMessage("Bad format : 3m3oquHQEoG0jAenBmlCjA");
   }

   @Test
   public void testParseWhenNoHash() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComposedHashFormat().parse("{pbkdf2hmacsha256;salt=Ft8qBQ;it=;kl=128}"))
            .withMessage("Bad format : {pbkdf2hmacsha256;salt=Ft8qBQ;it=;kl=128}");
   }

   @Test
   public void testParseWhenInvalidHash() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComposedHashFormat().parse(
                  "{pbkdf2hmacsha256;salt=Ft8qBQ;it=1000;kl=128}@!-------"))
            .withMessage("Bad format : {pbkdf2hmacsha256;salt=Ft8qBQ;it=1000;kl=128}@!-------");
   }

}