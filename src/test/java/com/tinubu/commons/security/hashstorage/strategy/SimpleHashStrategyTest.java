/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmBuilder;

class SimpleHashStrategyTest {

   @Test
   void testInstanceWhenNominal() {
      SimpleHashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithm()).isEqualTo(new Md5HashAlgorithmBuilder().build());
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build());
   }

   @Test
   void testInstanceWhenNullHashStrategy() {

      assertThatNullPointerException().isThrownBy(() -> new SimpleHashStrategy(null)).withMessage("'hashAlgorithm' must not be null");
   }

   @Test
   void testHashWhenNominal() {
      SimpleHashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hash(payload("payload"))).isEqualTo(new byte[] {
            50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
   }

   @Test
   void testHashWhenNullPayload() {
      SimpleHashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());

      assertThatNullPointerException().isThrownBy(() -> hashStrategy.hash(null)).withMessage("'payload' must not be null");
   }

   @Test
   void testHashWhenEmptyPayload() {
      SimpleHashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hash(payload(""))).isEqualTo(new byte[] {
            -44, 29, -116, -39, -113, 0, -78, 4, -23, -128, 9, -104, -20, -8, 66, 126 });
   }

   @Test
   public void testEquals() {
      SimpleHashStrategy hashStrategy1 = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
      SimpleHashStrategy hashStrategy2 = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
      SimpleHashStrategy hashStrategy3 = new SimpleHashStrategy(new Sha1HashAlgorithmBuilder().build());

      assertThat(hashStrategy1).isEqualTo(hashStrategy2);
      assertThat(hashStrategy1).hasSameHashCodeAs(hashStrategy2);

      assertThat(hashStrategy1).isNotEqualTo(hashStrategy3);
   }

   @Test
   public void testToString() {
      assertThat(new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build())).hasToString(
            "SimpleHashStrategy[hashAlgorithm=Md5HashAlgorithm[salt=null]]");
   }
}