/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static com.tinubu.commons.security.hashstorage.TestUtils.salt;
import static com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha256HashAlgorithm.Sha256HashAlgorithmBuilder;

class ComposedHashStrategyTest {

   @Test
   void testInstanceWhenNominal() {
      HashStrategy hashStrategy =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   void testInstanceWhenNullHashAlgorithm() {
      assertThatNullPointerException().isThrownBy(() -> new ComposedHashStrategy((HashAlgorithm) null)).withMessage("'hashAlgorithm' must not be null");
   }

   @Test
   void testInstanceWhenNullHashStrategy() {
      assertThatNullPointerException().isThrownBy(() -> new ComposedHashStrategy((SimpleHashStrategy) null)).withMessage("'hashStrategy' must not be null");
   }

   @Test
   void testInstanceOfHashAlgorithmsWhenNominal() {
      HashStrategy hashStrategy = ComposedHashStrategy.ofHashAlgorithms(new Sha1HashAlgorithmBuilder().build(),
                                                                        new Md5HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   void testInstanceOfHashAlgorithmsWhenNullHashAlgorithms() {
      assertThatNullPointerException()
            .isThrownBy(() -> ComposedHashStrategy.ofHashAlgorithms((List<HashAlgorithm>) null))
            .withMessage("'hashAlgorithms' must not be null");
   }

   @Test
   void testInstanceOfHashAlgorithmsWhenEmptyHashAlgorithms() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> ComposedHashStrategy.ofHashAlgorithms(emptyList()))
            .withMessage("'hashAlgorithms' must not be empty");
   }

   @Test
   void testInstanceOfHashAlgorithmsArrayWhenNullHashAlgorithms() {
      assertThatNullPointerException()
            .isThrownBy(() -> ComposedHashStrategy.ofHashAlgorithms((HashAlgorithm[]) null))
            .withMessage("'hashAlgorithms' must not be null");
   }

   @Test
   void testInstanceOfHashAlgorithmsArrayWhenEmptyHashAlgorithms() {
      assertThatIllegalArgumentException().isThrownBy(() -> ComposedHashStrategy.ofHashAlgorithms()).withMessage("'hashAlgorithms' must not be empty");
   }

   @Test
   void testComposeWhenNominal() {
      HashStrategy hashStrategy = new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build())
            .compose(new Sha1HashAlgorithmBuilder().build())
            .compose(new Sha256HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha256HashAlgorithmBuilder().build(),
                                                                new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   void testComposeWhenNullHashStrategy() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose((HashStrategy) null))
            .withMessage("'hashStrategy' must not be null");
   }

   @Test
   void testComposeWhenNullHashAlgorithm() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose((HashAlgorithm) null))
            .withMessage("'hashAlgorithm' must not be null");
   }

   @Test
   void testPreAppliedWhenNominal() {
      HashStrategy hashStrategy = new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build())
            .preApplied(new Sha1HashAlgorithmBuilder().build())
            .preApplied(new Sha256HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha256HashAlgorithmBuilder().build(),
                                                                new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   void testPreAppliedWhenNullHashStrategy() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).preApplied((HashStrategy) null))
            .withMessage("'hashStrategy' must not be null");
   }

   @Test
   void testPreAppliedWhenNullHashAlgorithm() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).preApplied((HashAlgorithm) null))
            .withMessage("'hashAlgorithm' must not be null");
   }

   @Test
   void testAndThenWhenNominal() {
      HashStrategy hashStrategy = new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build())
            .andThen(new Sha1HashAlgorithmBuilder().build())
            .andThen(new Sha256HashAlgorithmBuilder().build());

      assertThat(hashStrategy).isNotNull();
      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build(),
                                                                new Sha1HashAlgorithmBuilder().build(),
                                                                new Sha256HashAlgorithmBuilder().build());
   }

   @Test
   void testAndThenWhenNullHashStategy() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen((HashStrategy) null))
            .withMessage("'hashStrategy' must not be null");
   }

   @Test
   void testAndThenWhenNullHashAlgorithm() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen((HashAlgorithm) null))
            .withMessage("'hashAlgorithm' must not be null");
   }

   @Test
   void testHashWhenNominal() {
      HashStrategy hashStrategy =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hash(payload("payload"))).isEqualTo(new byte[] {
            25, 100, 4, -26, 66, 82, -124, -74, 48, -72, 41, -54, -68, -77, -40, -52 });
   }

   @Test
   void testHashWhenNullPayload() {
      HashStrategy hashStrategy =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());

      assertThatNullPointerException().isThrownBy(() -> hashStrategy.hash(null)).withMessage("'payload' must not be null");
   }

   @Test
   void testHashWhenEmptyPayload() {
      HashStrategy hashStrategy =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hash(payload(""))).isEqualTo(new byte[] {
            79, 113, -4, -54, -60, 60, 115, 84, 93, -35, -100, -41, 114, -13, 117, -104 });
   }

   @Test
   public void testEquals() {
      HashStrategy hashStrategy1 =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());
      HashStrategy hashStrategy2 =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());
      HashStrategy hashStrategy3 =
            new ComposedHashStrategy(new Sha1HashAlgorithmBuilder().build()).compose(new Md5HashAlgorithmBuilder().build());

      assertThat(hashStrategy1).isEqualTo(hashStrategy2);
      assertThat(hashStrategy1).hasSameHashCodeAs(hashStrategy2);

      assertThat(hashStrategy1).isNotEqualTo(hashStrategy3);
   }

   @Test
   public void testToString() {
      assertThat(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build())).hasToString(
            "ComposedHashStrategy[preAppliedHashStrategies=[], hashStrategies=[SimpleHashStrategy[hashAlgorithm=Sha1HashAlgorithm[salt=null]], SimpleHashStrategy[hashAlgorithm=Md5HashAlgorithm[salt=null]]]]");
   }

   @Test
   public void testHashAlgorithmsWhenNominal() {
      HashStrategy hashStrategy =
            new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).compose(new Sha1HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   public void testHashAlgorithmsWhenPreApplied() {
      HashStrategy hashStrategy = new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build())
            .compose(new Sha1HashAlgorithmBuilder().build())
            .preApplied(new Sha256HashAlgorithmBuilder().build());

      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha256HashAlgorithmBuilder().build(),
                                                                new Sha1HashAlgorithmBuilder().build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

   @Test
   public void testHashAlgorithmsWhenMultipleComposedHashStrategies() {
      HashStrategy hashStrategy = new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build())
            .compose(new ComposedHashStrategy(new Sha1HashAlgorithmBuilder()
                                                    .salt(salt("compose2"))
                                                    .build()).compose(new Sha1HashAlgorithmBuilder()
                                                                            .salt(salt("compose1"))
                                                                            .build()))
            .preApplied(new ComposedHashStrategy(new Sha1HashAlgorithmBuilder()
                                                       .salt(salt("preApplied2"))
                                                       .build()).compose(new Sha1HashAlgorithmBuilder()
                                                                               .salt(salt("preApplied1"))
                                                                               .build()));

      assertThat(hashStrategy.hashAlgorithms()).containsExactly(new Sha1HashAlgorithmBuilder()
                                                                      .salt(salt("preApplied1"))
                                                                      .build(),
                                                                new Sha1HashAlgorithmBuilder()
                                                                      .salt(salt("preApplied2"))
                                                                      .build(),
                                                                new Sha1HashAlgorithmBuilder()
                                                                      .salt(salt("compose1"))
                                                                      .build(),
                                                                new Sha1HashAlgorithmBuilder()
                                                                      .salt(salt("compose2"))
                                                                      .build(),
                                                                new Md5HashAlgorithmBuilder().build());
   }

}