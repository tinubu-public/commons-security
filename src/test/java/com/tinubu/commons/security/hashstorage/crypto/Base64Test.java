/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class Base64Test {

   @Nested
   public class Encode {

      @Test
      void testEncodeWhenNominal() {
         assertThat(Base64.encode(payload("payload"))).isEqualTo(new byte[] {
               99, 71, 70, 53, 98, 71, 57, 104, 90, 65, 61, 61 });
         assertThat(Base64.encode(payload("payload"), true)).isEqualTo(new byte[] {
               99, 71, 70, 53, 98, 71, 57, 104, 90, 65, 61, 61 });
         assertThat(Base64.encode(payload("payload"), false)).isEqualTo(new byte[] {
               99, 71, 70, 53, 98, 71, 57, 104, 90, 65 });
      }

      @Test
      void testEncodeWhenEmptyPayload() {
         assertThat(Base64.encode(payload(""))).isEqualTo(new byte[] {});
      }

      @Test
      void testEncodeWhenNullPayload() {
         assertThatNullPointerException().isThrownBy(() -> Base64.encode(null)).withMessage("'payload' must not be null");
      }
   }

   @Nested
   public class EncodeToString {

      @Test
      void testEncodeToStringWhenNominal() {
         assertThat(Base64.encodeToString(payload("payload"))).isEqualTo("cGF5bG9hZA==");
         assertThat(Base64.encodeToString(payload("payload"), true)).isEqualTo("cGF5bG9hZA==");
         assertThat(Base64.encodeToString(payload("payload"), false)).isEqualTo("cGF5bG9hZA");
      }

      @Test
      void testEncodeToStringWhenEmptyPayload() {
         assertThat(Base64.encodeToString(payload(""))).isEqualTo("");
      }

      @Test
      void testEncodeToStringWhenNullPayload() {
         assertThatNullPointerException().isThrownBy(() -> Base64.encodeToString(null)).withMessage("'payload' must not be null");
      }
   }

   @Nested
   public class DecodeString {
      @Test
      void testDecodeStringWhenNoPadding() {
         assertThat(Base64.decode("cGF5bG9hZA")).isEqualTo(payload("payload"));
      }

      @Test
      void testDecodeStringWhenPadding() {
         assertThat(Base64.decode("cGF5bG9hZA==")).isEqualTo(payload("payload"));
      }

      @Test
      void testDecodeStringWhenNullBase64() {
         assertThatNullPointerException().isThrownBy(() -> Base64.decode((String) null)).withMessage("'base64' must not be null");
      }

      @Test
      void testDecodeStringWhenEmptyBase64() {
         assertThat(Base64.decode("")).isEqualTo(payload(""));
      }

      @Test
      void testDecodeStringWhenInvalidBase64() {
         assertThatIllegalArgumentException().isThrownBy(() -> Base64.decode("AB!")).withMessage("Illegal base64 character 21");
      }
   }

   @Nested
   public class DecodeByteArray {
      @Test
      void testDecodeByteArrayWhenNoPadding() {
         assertThat(Base64.decode(new byte[] {
               99, 71, 70, 53, 98, 71, 57, 104, 90, 65 })).isEqualTo(payload("payload"));
      }

      @Test
      void testDecodeByteArrayWhenPadding() {
         assertThat(Base64.decode(new byte[] { 99, 71, 70, 53, 98, 71, 57, 104, 90, 65, 61, 61 })).isEqualTo(
               payload("payload"));
      }

      @Test
      void testDecodeByteArrayWhenNullBase64() {
         assertThatNullPointerException().isThrownBy(() -> Base64.decode((byte[]) null)).withMessage("'base64' must not be null");
      }

      @Test
      void testDecodeByteArrayWhenEmptyBase64() {
         assertThat(Base64.decode(new byte[] {})).isEqualTo(payload(""));
      }

      @Test
      void testDecodeByteArrayWhenInvalidBase64() {
         assertThatIllegalArgumentException().isThrownBy(() -> Base64.decode(payload("AB!"))).withMessage("Illegal base64 character 21");
      }

   }

}