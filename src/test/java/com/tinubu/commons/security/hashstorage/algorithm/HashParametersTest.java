/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.algorithm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.HashParameters.HashParametersBuilder;

class HashParametersTest {

   @Test
   public void testInstanceWhenNominal() {
      assertThat(new HashParametersBuilder().build()).isNotNull();
   }

   @Test
   public void testInstanceWhenNullParameters() {
      assertThatNullPointerException().isThrownBy(() -> new HashParametersBuilder().parameters(null).build()).withMessage("'parameters' must not be null");
   }

   @Test
   public void testParametersOrder() {
      HashParameters parameters = new HashParametersBuilder()
            .parameter("a", "A")
            .parameter("d", "D")
            .parameter("b", "B")
            .parameter("c", "C")
            .build();

      assertThat(parameters.keySet()).containsExactly("a", "d", "b", "c");
   }

}