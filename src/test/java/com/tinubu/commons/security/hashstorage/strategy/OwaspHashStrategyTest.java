/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.Salt;

class OwaspHashStrategyTest {

   @Test
   void testPbkdf2InstanceWhenNominal() {
      HashStrategy hashStrategy = OwaspHashStrategy.pbkdf2();

      assertThat(hashStrategy).isNotNull();
   }

   @Test
   void testPbkdf2InstanceWhenBadSalt() {
      assertThatIllegalArgumentException().isThrownBy(() -> OwaspHashStrategy.pbkdf2(Salt.pseudoRandom(10))).withMessage("Salt length must be >= 16");
   }

   @Test
   public void testEquals() {
      Salt salt = Salt.pseudoRandom(16);
      HashStrategy hashStrategy1 = OwaspHashStrategy.pbkdf2(salt);
      HashStrategy hashStrategy2 = OwaspHashStrategy.pbkdf2(salt);

      assertThat(hashStrategy1).isEqualTo(hashStrategy2);
      assertThat(hashStrategy1).hasSameHashCodeAs(hashStrategy2);
   }

   @Test
   public void testToString() {
      assertThat(OwaspHashStrategy.pbkdf2(Salt.ofString("01234567890123456"))).hasToString(
            "OwaspHashStrategy[hashAlgorithm=Pbkdf2HmacSha256HashAlgorithm[salt=Salt[salt=[48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 49, 50, 51, 52, 53, 54]], iterations=310000, keyLength=512, payloadCharset=UTF-8]]");
   }

}