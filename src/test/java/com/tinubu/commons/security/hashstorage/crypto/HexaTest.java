/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class HexaTest {

   @Nested
   public class Encode {

      @Test
      void testEncodeWhenNominal() {
         assertThat(Hexa.encode(payload("payload"))).isEqualTo(new char[] {
               '7', '0', '6', '1', '7', '9', '6', 'c', '6', 'f', '6', '1', '6', '4' });
         assertThat(Hexa.encode(payload("payload"), true)).isEqualTo(new char[] {
               '7', '0', '6', '1', '7', '9', '6', 'c', '6', 'f', '6', '1', '6', '4' });
         assertThat(Hexa.encode(payload("payload"), false)).isEqualTo(new char[] {
               '7', '0', '6', '1', '7', '9', '6', 'C', '6', 'F', '6', '1', '6', '4' });
      }

      @Test
      void testEncodeWhenEmptyPayload() {
         assertThat(Hexa.encode(payload(""))).isEqualTo(new char[] {});
      }

      @Test
      void testEncodeWhenNullPayload() {
         assertThatNullPointerException().isThrownBy(() -> Hexa.encode(null)).withMessage("'payload' must not be null");
      }
   }

   @Nested
   public class EncodeToString {

      @Test
      void testEncodeToStringWhenNominal() {
         assertThat(Hexa.encodeToString(payload("payload"))).isEqualTo("7061796c6f6164");
         assertThat(Hexa.encodeToString(payload("payload"), true)).isEqualTo("7061796c6f6164");
         assertThat(Hexa.encodeToString(payload("payload"), false)).isEqualTo("7061796C6F6164");
      }

      @Test
      void testEncodeToStringWhenEmptyPayload() {
         assertThat(Hexa.encodeToString(payload(""))).isEqualTo("");
      }

      @Test
      void testEncodeToStringWhenNullPayload() {
         assertThatNullPointerException().isThrownBy(() -> Hexa.encodeToString(null)).withMessage("'payload' must not be null");
      }
   }

   @Nested
   public class DecodeString {
      @Test
      void testDecodeStringWhenLowerCase() {
         assertThat(Hexa.decode("7061796c6f6164")).isEqualTo(payload("payload"));
      }

      @Test
      void testDecodeStringWhenUpperCase() {
         assertThat(Hexa.decode("7061796C6F6164")).isEqualTo(payload("payload"));
      }

      @Test
      void testDecodeStringWhenNullHexa() {
         assertThatNullPointerException().isThrownBy(() -> Hexa.decode((String) null)).withMessage("'hex' must not be null");
      }

      @Test
      void testDecodeStringWhenEmptyHexa() {
         assertThat(Hexa.decode("")).isEqualTo(payload(""));
      }

      @Test
      void testDecodeStringWhenInvalidHexa() {
         assertThatIllegalArgumentException().isThrownBy(() -> Hexa.decode("7z")).withMessage("Illegal hexadecimal character z at index 1");
      }

   }

   @Nested
   public class DecodeByteArray {
      @Test
      void testDecodeCharArrayWhenLowerCase() {
         assertThat(Hexa.decode(new char[] {
               '7', '0', '6', '1', '7', '9', '6', 'c', '6', 'f', '6', '1', '6', '4' })).isEqualTo(payload(
               "payload"));
      }

      @Test
      void testDecodeCharArrayWhenUpperCase() {
         assertThat(Hexa.decode(new char[] {
               '7', '0', '6', '1', '7', '9', '6', 'C', '6', 'F', '6', '1', '6', '4' })).isEqualTo(payload(
               "payload"));
      }

      @Test
      void testDecodeCharArrayWhenNullHexa() {
         assertThatNullPointerException().isThrownBy(() -> Hexa.decode((char[]) null)).withMessage("'hex' must not be null");
      }

      @Test
      void testDecodeByteArrayWhenEmptyHexa() {
         assertThat(Hexa.decode(new char[] {})).isEqualTo(payload(""));
      }

      @Test
      void testDecodeCharArrayWhenInvalidHexa() {
         assertThatIllegalArgumentException().isThrownBy(() -> Hexa.decode(new char[] { '7', 'z' })).withMessage("Illegal hexadecimal character z at index 1");
      }

   }

}