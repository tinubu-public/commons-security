/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.algorithm.BcPbkdf2HmacSha256HashAlgorithm.BcPbkdf2HmacSha256HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.BcPbkdf2HmacSha256HashAlgorithm.BcPbkdf2HmacSha256HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithm;
import com.tinubu.commons.security.hashstorage.algorithm.HashAlgorithmRegistry;
import com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmProvider;
import com.tinubu.commons.security.hashstorage.algorithm.Salt;
import com.tinubu.commons.security.hashstorage.algorithm.Sha1HashAlgorithm.Sha1HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha256HashAlgorithm.Sha256HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.algorithm.Sha512HashAlgorithm.Sha512HashAlgorithmBuilder;
import com.tinubu.commons.security.hashstorage.format.ComposedHashFormat;
import com.tinubu.commons.security.hashstorage.strategy.ComposedHashStrategy;
import com.tinubu.commons.security.hashstorage.strategy.HashStrategy;
import com.tinubu.commons.security.hashstorage.strategy.SimpleHashStrategy;

public class HashStorageTest {

   @BeforeEach
   public void beforeEach() {
      HashAlgorithmRegistry registry = HashAlgorithmRegistry.instance();

      registry.clearRegisteredHashAlgorithmProviders();
      registry.registerHashAlgorithm(new Md5HashAlgorithmProvider());
      registry.registerHashAlgorithm(new BcPbkdf2HmacSha256HashAlgorithmProvider());
   }

   @Nested
   public class General {

      @Test
      public void testUsedHashAlgorithmsWhenNominal() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(
                     new Sha1HashAlgorithmBuilder().build()), payload("payload"));

         assertThat(hashStorage.usesHashAlgorithms(Arrays.asList(new Md5HashAlgorithmBuilder().build(),
                                                                 new Sha1HashAlgorithmBuilder().build()))).isTrue();
         assertThat(hashStorage.usesHashAlgorithms(Arrays.asList(new Md5HashAlgorithmBuilder().build(),
                                                                 new Sha256HashAlgorithmBuilder().build()))).isFalse();
         assertThat(hashStorage.usesHashAlgorithms(singletonList(new Md5HashAlgorithmBuilder().build()))).isFalse();
      }

      @Test
      public void testUsedHashAlgorithmsWhenBadParameters() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(
                     new Sha1HashAlgorithmBuilder().build()), payload("payload"));

         assertThatIllegalArgumentException().isThrownBy(() -> hashStorage.usesHashAlgorithms(emptyList())).withMessage("'hashAlgorithms' must not be empty");
         assertThatNullPointerException()
               .isThrownBy(() -> hashStorage.usesHashAlgorithms((List<HashAlgorithm>) null))
               .withMessage("'hashAlgorithms' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> hashStorage.usesHashAlgorithms(Arrays.asList(null, null)))
               .withMessage("'hashAlgorithms' must not have null elements at index : 0");
      }

      @Test
      public void testUsedHashAlgorithmsWithVarargs() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(
                     new Sha1HashAlgorithmBuilder().build()), payload("payload"));

         assertThat(hashStorage.usesHashAlgorithms(new Md5HashAlgorithmBuilder().build(),
                                                   new Sha1HashAlgorithmBuilder().build())).isTrue();
         assertThat(hashStorage.usesHashAlgorithms(new Md5HashAlgorithmBuilder().build(),
                                                   new Sha256HashAlgorithmBuilder().build())).isFalse();
         assertThat(hashStorage.usesHashAlgorithms(new Md5HashAlgorithmBuilder().build())).isFalse();
      }

      @Test
      public void testUsedHashAlgorithmsWithVarargsWhenBadParameters() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(
                     new Sha1HashAlgorithmBuilder().build()), payload("payload"));

         assertThatIllegalArgumentException().isThrownBy(() -> hashStorage.usesHashAlgorithms()).withMessage("'hashAlgorithms' must not be empty");
         assertThatNullPointerException()
               .isThrownBy(() -> hashStorage.usesHashAlgorithms((HashAlgorithm[]) null))
               .withMessage("'hashAlgorithms' must not be null");
         assertThatIllegalArgumentException().isThrownBy(() -> hashStorage.usesHashAlgorithms(null, null)).withMessage("'hashAlgorithms' must not have null elements at index : 0");
      }

   }

   @Nested
   public class InstanceOfHash {

      @Test
      public void testInstanceOfHashWhenNominal() {
         HashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
         HashStorage hashStorage = HashStorage.ofHash(hashStrategy, new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
         assertThat(hashStorage.hashAlgorithms()).containsExactlyElementsOf(hashStrategy.hashAlgorithms());
      }

      @Test
      public void testInstanceOfHashWhenNullStrategy() {
         assertThatNullPointerException().isThrownBy(() -> HashStorage.ofHash((HashStrategy) null, new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 })).withMessage("'hashStrategy' must not be null");
      }

      @Test
      public void testInstanceOfHashWhenEmptyHash() {
         HashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());

         assertThatNullPointerException().isThrownBy(() -> HashStorage.ofHash(hashStrategy, null)).withMessage("'hash' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> HashStorage.ofHash(hashStrategy, new byte[] {}))
               .withMessage("'hash' must not be empty");
      }

      @Test
      public void testInstanceOfHashWhenHashAlgorithm() {
         HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();
         HashStorage hashStorage = HashStorage.ofHash(hashAlgorithm, new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
         assertThat(hashStorage.hashAlgorithms()).containsExactly(hashAlgorithm);
      }

      @Test
      public void testInstanceOfHashWhenNullHashAlgorithm() {
         assertThatNullPointerException().isThrownBy(() -> HashStorage.ofHash((HashAlgorithm) null, new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 })).withMessage("'hashAlgorithm' must not be null");
      }

   }

   @Nested
   public class InstanceOfPayload {

      @Test
      public void testInstanceOfPayloadWhenNominal() {
         HashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
         HashStorage hashStorage = HashStorage.ofPayload(hashStrategy, payload("payload"));

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
         assertThat(hashStorage.hashAlgorithms()).containsExactlyElementsOf(hashStrategy.hashAlgorithms());
      }

      @Test
      public void testInstanceOfPayloadWhenNullHashStrategy() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage.ofPayload((HashStrategy) null, payload("payload")))
               .withMessage("'hashStrategy' must not be null");
      }

      @Test
      public void testInstanceOfPayloadWhenNullPayload() {
         HashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
         assertThatNullPointerException().isThrownBy(() -> HashStorage.ofPayload(hashStrategy, null)).withMessage("'payload' must not be null");
      }

      @Test
      public void testInstanceOfPayloadWhenEmptyPayload() {
         HashStrategy hashStrategy = new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build());
         HashStorage hashStorage = HashStorage.ofPayload(hashStrategy, payload(""));

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               -44, 29, -116, -39, -113, 0, -78, 4, -23, -128, 9, -104, -20, -8, 66, 126 });
         assertThat(hashStorage.hashAlgorithms()).containsExactlyElementsOf(hashStrategy.hashAlgorithms());
      }

      @Test
      public void testInstanceOfPayloadWhenHashAlgorithm() {
         HashAlgorithm hashAlgorithm = new Md5HashAlgorithmBuilder().build();
         HashStorage hashStorage = HashStorage.ofPayload(hashAlgorithm, payload("payload"));

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
         assertThat(hashStorage.hashAlgorithms()).containsExactly(hashAlgorithm);
      }

      @Test
      public void testInstanceOfPayloadWhenNullHashAlgorithm() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage.ofPayload((HashAlgorithm) null, payload("payload")))
               .withMessage("'hashAlgorithm' must not be null");
      }

   }

   @Nested
   public class InstanceOfFormat {

      @Test
      public void testInstanceOfFormatWhenNominal() {
         HashStorage hashStorage =
               HashStorage.ofFormat(new ComposedHashFormat(), "{md5}Mhw89IbtUJFk7eweGYH+yA");

         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               50, 28, 60, -12, -122, -19, 80, -111, 100, -19, -20, 30, 25, -127, -2, -56 });
         assertThat(hashStorage.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build());
      }

      @Test
      public void testInstanceOfFormatWhenNullHashFormat() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage.ofFormat(null, "{md5}Mhw89IbtUJFk7eweGYH+yA"))
               .withMessage("'hashFormat' must not be null");

      }

      @Test
      public void testInstanceOfFormatWhenBlankFormat() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage.ofFormat(new ComposedHashFormat(), null))
               .withMessage("'format' must not be null");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> HashStorage.ofFormat(new ComposedHashFormat(), ""))
               .withMessage("'format' must not be blank");
         assertThatIllegalArgumentException()
               .isThrownBy(() -> HashStorage.ofFormat(new ComposedHashFormat(), " "))
               .withMessage("'format' must not be blank");
      }

   }

   @Nested
   public class EqualityRules {

      @Test
      public void testEquals() {
         HashStorage hashStorage1 =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));
         HashStorage hashStorage2 =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));
         HashStorage hashStorage3 =
               HashStorage.ofPayload(new Sha1HashAlgorithmBuilder().build(), payload("payload"));
         HashStorage hashStorage4 =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("different"));

         assertThat(hashStorage1).isEqualTo(hashStorage2);
         assertThat(hashStorage1).hasSameHashCodeAs(hashStorage2);

         assertThat(hashStorage1).isNotEqualTo(hashStorage3);
         assertThat(hashStorage1).isNotEqualTo(hashStorage4);
      }
   }

   @Nested
   public class Verify {

      @Test
      public void testVerifyByPayloadWhenNominal() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThat(hashStorage.verify(payload("payload"))).isTrue();
         assertThat(hashStorage.verify(payload("different"))).isFalse();
      }

      @Test
      public void testVerifyByPayloadWhenEmptyPayload() {
         HashStorage hashStorage = HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload(""));

         assertThat(hashStorage.verify(payload(""))).isTrue();
      }

      @Test
      public void testVerifyByPayloadWhenNullPayload() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThatNullPointerException().isThrownBy(() -> hashStorage.verify((byte[]) null)).withMessage("'payload' must not be null");
      }

      @Test
      public void testVerifyByHashStorageWhenNominal() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThat(hashStorage.verify(HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(),
                                                             payload("payload")))).isTrue();
         assertThat(hashStorage.verify(HashStorage.ofPayload(new Sha1HashAlgorithmBuilder().build(),
                                                             payload("payload")))).isFalse();
         assertThat(hashStorage.verify(HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(),
                                                             payload("different")))).isFalse();
      }

      @Test
      public void testVerifyByPayloadWhenNullHashStorage() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThatNullPointerException().isThrownBy(() -> hashStorage.verify((HashStorage) null)).withMessage("'hashStorage' must not be null");
      }

   }

   @Nested
   public class Format {

      @Test
      public void testFormatWhenNominal() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThat(hashStorage.format(new ComposedHashFormat())).isEqualTo("{md5}Mhw89IbtUJFk7eweGYH+yA");
      }

      @Test
      public void testFormatWhenNullFormat() {
         HashStorage hashStorage =
               HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

         assertThatNullPointerException().isThrownBy(() -> hashStorage.format(null)).withMessage("'format' must not be null");
      }
   }

   @Nested
   public class Rehash {

      @Test
      public void testRehashWhenNominal() {
         HashStorage hashStorage = HashStorage
               .ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"))
               .rehash(new SimpleHashStrategy(new Sha1HashAlgorithmBuilder().build()));

         assertThat(hashStorage.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build(),
                                                                  new Sha1HashAlgorithmBuilder().build());
         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               -128, 61, 122, 61, 18, 64, -66, -28, -2, -72, -31, 86, -50, -71, -45, -23, 67, 92, -98, -50 });
      }

      @Test
      public void testRehashWhenNullHashStrategy() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage
                     .ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"))
                     .rehash((HashStrategy) null))
               .withMessage("'hashStrategy' must not be null");
      }

      @Test
      public void testRehashWhenHashAlgorithm() {
         HashStorage hashStorage = HashStorage
               .ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"))
               .rehash(new Sha1HashAlgorithmBuilder().build());

         assertThat(hashStorage.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build(),
                                                                  new Sha1HashAlgorithmBuilder().build());
         assertThat(hashStorage.hash()).isEqualTo(new byte[] {
               -128, 61, 122, 61, 18, 64, -66, -28, -2, -72, -31, 86, -50, -71, -45, -23, 67, 92, -98, -50 });
      }

      @Test
      public void testRehashWhenNullHashAlgorithm() {
         assertThatNullPointerException()
               .isThrownBy(() -> HashStorage
                     .ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"))
                     .rehash((HashAlgorithm) null))
               .withMessage("'hashAlgorithm' must not be null");
      }

      @Test
      public void testRehashWhenComposedHashStrategy() {
         HashStorage hashStorage = HashStorage
               .ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(new Sha1HashAlgorithmBuilder().build()),
                          payload("payload"))
               .rehash(new ComposedHashStrategy(new Sha256HashAlgorithmBuilder().build()).andThen(new Sha512HashAlgorithmBuilder().build()));

         assertThat(hashStorage.hashAlgorithms()).containsExactly(new Md5HashAlgorithmBuilder().build(),
                                                                  new Sha1HashAlgorithmBuilder().build(),
                                                                  new Sha256HashAlgorithmBuilder().build(),
                                                                  new Sha512HashAlgorithmBuilder().build());
         assertThat(hashStorage.hash()).isEqualTo(TestUtils.hash(payload("payload"),
                                                                 new Md5HashAlgorithmBuilder().build(),
                                                                 new Sha1HashAlgorithmBuilder().build(),
                                                                 new Sha256HashAlgorithmBuilder().build(),
                                                                 new Sha512HashAlgorithmBuilder().build()));
      }
   }

   @Nested
   public class ArchitecturalDesign {

      /**
       * HashStorage should always be able to verify another hash storage, based only on the final applied
       * hash algorithms, not the hash strategies used and theirs implementations.
       */
      @Test
      public void testVerifyAgnosticToHashStrategyLogic() {
         HashStorage hashStorage1 =
               HashStorage.ofPayload(new ComposedHashStrategy(new Md5HashAlgorithmBuilder().build()).andThen(
                     new Sha1HashAlgorithmBuilder().build()), payload("payload"));
         HashStorage hashStorage2 = HashStorage
               .ofPayload(new SimpleHashStrategy(new Md5HashAlgorithmBuilder().build()), payload("payload"))
               .rehash(new Sha1HashAlgorithmBuilder().build());

         assertThat(hashStorage1.verify(hashStorage2)).isTrue();
      }
   }

   @Nested
   public class ThreadSafety {

      private static final int NB_THREADS = 10;
      private static final int NB_ITERATIONS = 10000;

      @Test
      public void testHashStorageThreadSafety() {

         List<Thread> threads = IntStream.range(0, NB_THREADS).boxed().map(i -> {
            String payload = String.format("payload-%d", i);
            ComposedHashStrategy hashStrategy =
                  new ComposedHashStrategy(new Sha1HashAlgorithmBuilder().build()).andThen(new BcPbkdf2HmacSha256HashAlgorithmBuilder()
                                                                                                 .iterations(1)
                                                                                                 .salt(Salt.pseudoRandom(
                                                                                                       4))
                                                                                                 .build());
            String format =
                  HashStorage.ofPayload(hashStrategy, payload(payload)).format(new ComposedHashFormat());

            return new Thread(verify(payload, format));
         }).collect(toList());

         threads.forEach(t -> {
            t.start();
            try {
               t.join(0);
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();
            }
         });
      }

      private Runnable verify(final String payload, final String format) {
         return () -> {
            for (int i = 0; i < NB_ITERATIONS; i++) {
               assertThat(HashStorage
                                .ofFormat(new ComposedHashFormat(), format)
                                .verify(payload(payload))).isTrue();
            }
         };
      }

   }
}
