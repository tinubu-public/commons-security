/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.crypto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.junit.jupiter.api.Test;

class RandomTest {

   @Test
   public void testRandomInstanceWhenNominal() {
      assertThat(new Random().random(8)).hasSize(8);
   }

   @Test
   public void testRandomInstanceWhenSecureRandom() throws NoSuchAlgorithmException {
      assertThat(new Random(SecureRandom.getInstance("SHA1PRNG"))).isNotNull();
   }

   @Test
   public void testRandomInstanceWhenProviderName() {
      assertThat(new Random("SHA1PRNG")).isNotNull();
   }

   @Test
   public void testRandomInstanceWhenNullProviderName() {
      assertThatNullPointerException().isThrownBy(() -> new Random((String) null)).withMessage("'providerName' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> new Random(" ")).withMessage("'providerName' must not be blank");
   }

   @Test
   public void testRandomInstanceWhenMissingProviderName() {
      assertThatIllegalArgumentException().isThrownBy(() -> new Random("MISSING"));
   }

   @Test
   public void testRandomWhenNominal() {
      assertThat(new Random().random(8)).hasSize(8);
   }

   @Test
   public void testRandomWhenZeroSize() {
      assertThat(new Random().random(0)).hasSize(0);
   }

   @Test
   public void testRandomWhenNegativeSize() {
      assertThatExceptionOfType(NegativeArraySizeException.class).isThrownBy(() -> new Random().random(-1));
   }
}