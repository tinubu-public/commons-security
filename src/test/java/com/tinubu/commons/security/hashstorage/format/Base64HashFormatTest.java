/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.security.hashstorage.format;

import static com.tinubu.commons.security.hashstorage.TestUtils.payload;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.security.hashstorage.HashStorage;
import com.tinubu.commons.security.hashstorage.algorithm.Md5HashAlgorithm.Md5HashAlgorithmBuilder;

class Base64HashFormatTest {

   @Test
   public void testFormatWhenNominal() {
      HashStorage hashStorage =
            HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

      Base64HashFormat hashFormat = new Base64HashFormat();
      assertThat(hashFormat.supportMultipleHashAlgorithms()).isTrue();
      assertThat(hashFormat.format(hashStorage)).isEqualTo("Mhw89IbtUJFk7eweGYH+yA");
   }

   @Test
   public void testFormatWhenNullHashStorage() {
      assertThatNullPointerException().isThrownBy(() -> new Base64HashFormat().format(null)).withMessage("'hashStorage' must not be null");
   }

   @Test
   public void testFormatWhenPadding() {
      HashStorage hashStorage =
            HashStorage.ofPayload(new Md5HashAlgorithmBuilder().build(), payload("payload"));

      assertThat(new Base64HashFormat(false).format(hashStorage)).isEqualTo("Mhw89IbtUJFk7eweGYH+yA");
      assertThat(new Base64HashFormat(true).format(hashStorage)).isEqualTo("Mhw89IbtUJFk7eweGYH+yA==");
   }

   @Test
   public void testParseWhenNominal() {
      assertThatExceptionOfType(UnsupportedOperationException.class).isThrownBy(() -> new Base64HashFormat().parse(
            "Mhw89IbtUJFk7eweGYH+yA"));
   }

   @Test
   public void testParseWhenBlankFormat() {
      assertThatNullPointerException().isThrownBy(() -> new Base64HashFormat().parse(null)).withMessage("'hashFormat' must not be null");
      assertThatIllegalArgumentException().isThrownBy(() -> new Base64HashFormat().parse("")).withMessage("'hashFormat' must not be blank");
      assertThatIllegalArgumentException().isThrownBy(() -> new Base64HashFormat().parse(" ")).withMessage("'hashFormat' must not be blank");
   }

}